#!/usr/bin/env python3
# Copyright (c) 2018 The Dash Core developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.
import time

from test_framework.test_framework import BitcoinTestFramework
from test_framework.util import connect_nodes, set_node_times, wait_until

'''
multikeysporks.py

Test logic for several signer keys usage for spork broadcast.

We set 5 possible keys for sporks signing and set minimum 
required signers to 3. We check 1 and 2 signers can't set the spork 
value, any 3 signers can change spork value and other 3 signers
can change it again.
'''


class MultiKeySporkTest(BitcoinTestFramework):
    def set_test_params(self):
        self.num_nodes = 5
        self.setup_clean_chain = True
        self.is_network_split = False

    def setup_network(self):
        # secret(base58): cUbvv8EZpZKR2x6EayHEbMwYDhqeqyBPz9eHS2BATHmiN4HBgayT
        # keyid(hex): 60f0f57f71f0081f1aacdd8432340a33a526f91b
        # address(base58): gS6VjjtwEqBpW44DNeh2at6eR3ADreV3aY

        # secret(base58): cNyqqsx7XsbT6xZXDJMgGeTDkKgK7GgchgAYZsunsP7W4qXGZez7
        # keyid(hex): 43dff2b09de2f904f688ec14ee6899087b889ad0
        # address(base58): gBzc2BKQDiJADzU4V9CJvMWXQkm2jsmqSC

        # secret(base58): cPMjZjmQZf3Wg5PhQbrTAeoh4JbSqCRk7ntLAf6oN2umCVSQ78DZ
        # keyid(hex): d9aa5fa00cce99101a4044e65dc544d1579890de
        # address(base58): gRd8PtjGrSRA8vrASoUz66wu6zsUBW9FX9

        # secret(base58): cSp4RdiRZawuj2zW3ymmJg2nYeVzoqDMaE34EBzvVWeusZ9qgy66
        # keyid(hex): 0b23935ce0bea3b997a334f6fa276c9fa17687b2
        # address(base58): gHXgk3RJUqg7Tcdk21ujAdJwpcLs7izoFS

        # secret(base58): cTVUeT7qqT2YHGkqcBHMTPebmtu22Bq2g4inX7a7jav6nuxhwG6D
        # keyid(hex): 1d1098b2b1f759b678a0a7a098637a9b898adcac
        # address(base58): gHqDYEo3dt8Kq4HV4V8Q11jjTgma4RzHpP

        self.add_nodes(5)

        self.start_node(0, ["-sporkkey=cUbvv8EZpZKR2x6EayHEbMwYDhqeqyBPz9eHS2BATHmiN4HBgayT",
                            "-sporkaddr=gRd8PtjGrSRA8vrASoUz66wu6zsUBW9FX9",
                            "-sporkaddr=gBzc2BKQDiJADzU4V9CJvMWXQkm2jsmqSC",
                            "-sporkaddr=gS6VjjtwEqBpW44DNeh2at6eR3ADreV3aY",
                            "-sporkaddr=gHXgk3RJUqg7Tcdk21ujAdJwpcLs7izoFS",
                            "-sporkaddr=gHqDYEo3dt8Kq4HV4V8Q11jjTgma4RzHpP",
                            "-minsporkkeys=3"])
        self.start_node(1, ["-sporkkey=cNyqqsx7XsbT6xZXDJMgGeTDkKgK7GgchgAYZsunsP7W4qXGZez7",
                            "-sporkaddr=gRd8PtjGrSRA8vrASoUz66wu6zsUBW9FX9",
                            "-sporkaddr=gBzc2BKQDiJADzU4V9CJvMWXQkm2jsmqSC",
                            "-sporkaddr=gS6VjjtwEqBpW44DNeh2at6eR3ADreV3aY",
                            "-sporkaddr=gHXgk3RJUqg7Tcdk21ujAdJwpcLs7izoFS",
                            "-sporkaddr=gHqDYEo3dt8Kq4HV4V8Q11jjTgma4RzHpP",
                            "-minsporkkeys=3"])
        self.start_node(2, ["-sporkkey=cPMjZjmQZf3Wg5PhQbrTAeoh4JbSqCRk7ntLAf6oN2umCVSQ78DZ",
                            "-sporkaddr=gRd8PtjGrSRA8vrASoUz66wu6zsUBW9FX9",
                            "-sporkaddr=gBzc2BKQDiJADzU4V9CJvMWXQkm2jsmqSC",
                            "-sporkaddr=gS6VjjtwEqBpW44DNeh2at6eR3ADreV3aY",
                            "-sporkaddr=gHXgk3RJUqg7Tcdk21ujAdJwpcLs7izoFS",
                            "-sporkaddr=gHqDYEo3dt8Kq4HV4V8Q11jjTgma4RzHpP",
                            "-minsporkkeys=3"])
        self.start_node(3, ["-sporkkey=cSp4RdiRZawuj2zW3ymmJg2nYeVzoqDMaE34EBzvVWeusZ9qgy66",
                            "-sporkaddr=gRd8PtjGrSRA8vrASoUz66wu6zsUBW9FX9",
                            "-sporkaddr=gBzc2BKQDiJADzU4V9CJvMWXQkm2jsmqSC",
                            "-sporkaddr=gS6VjjtwEqBpW44DNeh2at6eR3ADreV3aY",
                            "-sporkaddr=gHXgk3RJUqg7Tcdk21ujAdJwpcLs7izoFS",
                            "-sporkaddr=gHqDYEo3dt8Kq4HV4V8Q11jjTgma4RzHpP",
                            "-minsporkkeys=3"])
        self.start_node(4, ["-sporkkey=cTVUeT7qqT2YHGkqcBHMTPebmtu22Bq2g4inX7a7jav6nuxhwG6D",
                            "-sporkaddr=gRd8PtjGrSRA8vrASoUz66wu6zsUBW9FX9",
                            "-sporkaddr=gBzc2BKQDiJADzU4V9CJvMWXQkm2jsmqSC",
                            "-sporkaddr=gS6VjjtwEqBpW44DNeh2at6eR3ADreV3aY",
                            "-sporkaddr=gHXgk3RJUqg7Tcdk21ujAdJwpcLs7izoFS",
                            "-sporkaddr=gHqDYEo3dt8Kq4HV4V8Q11jjTgma4RzHpP",
                            "-minsporkkeys=3"])
        # connect nodes at start
        for i in range(0, 5):
            for j in range(i, 5):
                connect_nodes(self.nodes[i], j)

    def get_test_spork_value(self, node):
        info = node.spork('show')
        # use InstantSend spork for tests
        return info['SPORK_12_INSTANTSEND_ENABLED']

    def set_test_spork_value(self, node, value):
        # use InstantSend spork for tests
        node.spork('SPORK_12_INSTANTSEND_ENABLED', value)

    def run_test(self):
        # check test spork default state
        for node in self.nodes:
            assert(self.get_test_spork_value(node) == 4070908800)

        self.bump_mocktime(1)
        set_node_times(self.nodes, self.mocktime)
        # first and second signers set spork value
        self.set_test_spork_value(self.nodes[0], 1)
        self.set_test_spork_value(self.nodes[1], 1)
        # spork change requires at least 3 signers
        time.sleep(10)
        for node in self.nodes:
            assert(self.get_test_spork_value(node) != 1)

        # third signer set spork value
        self.set_test_spork_value(self.nodes[2], 1)
        # now spork state is changed
        for node in self.nodes:
            wait_until(lambda: self.get_test_spork_value(node) == 1, sleep=0.1, timeout=10)

        self.bump_mocktime(1)
        set_node_times(self.nodes, self.mocktime)
        # now set the spork again with other signers to test
        # old and new spork messages interaction
        self.set_test_spork_value(self.nodes[2], 2)
        self.set_test_spork_value(self.nodes[3], 2)
        self.set_test_spork_value(self.nodes[4], 2)
        for node in self.nodes:
            wait_until(lambda: self.get_test_spork_value(node) == 2, sleep=0.1, timeout=10)


if __name__ == '__main__':
    MultiKeySporkTest().main()
